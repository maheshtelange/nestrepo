package com.qmetry.qaf.example.bean;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.Randomizer;

public class ExpenseDetailsBean extends BaseDataBean {

	@Randomizer(skip = true)
	private String expenseTitle;

	@Randomizer(skip = true)
	private String projectTitle;

	@Randomizer(skip = true)
	private String expenseCategory;

	@Randomizer(skip = true)
	private String expenseAmount;

	@Randomizer(skip = true)
	private String projectName;

	public String getExpenseTitle() {
		return expenseTitle;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setExpenseTitle(String expenseTitle) {
		this.expenseTitle = expenseTitle;
	}

	public String getProjectTitle() {
		return expenseTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public String getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(String expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public String getExpenseCategory() {
		return expenseCategory;
	}

	public void setExpenseCategory(String expenseCategory) {
		this.expenseCategory = expenseCategory;
	}
}