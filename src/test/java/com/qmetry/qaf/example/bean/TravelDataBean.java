package com.qmetry.qaf.example.bean;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.Randomizer;

public class TravelDataBean extends BaseDataBean {

	@Randomizer(skip = true)
	private String travelTitle;

	public String getTravelTitle() {
		return travelTitle;
	}

	public void setTravelTitle(String travelTitle) {
		this.travelTitle = travelTitle;
	}
}
