package com.qmetry.qaf.example.component;

import java.util.List;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class MenuListItem extends QAFWebComponent{

	
	
	@FindBy(locator = "home.menuList.link")
	private QAFWebElement linkText;

	
	@FindBy(locator = "home.subMenuList.link")
	private List<SubMenuList> subMenuList;

	public MenuListItem(String locator) {
		super(locator);
	}
	
	

	public QAFWebElement getLinkText() {
		return linkText;
	}

	public List<SubMenuList> getSubMenuList() {
		return subMenuList;
	}


	public class SubMenuList extends QAFWebComponent {

		@FindBy(locator = "home.subMenuLinkList.link")
		private QAFWebElement subMenuLinkText;

		public SubMenuList(String locator) {
			super(locator);
		}

		public QAFWebElement getSubMenuLinkText() {
			return subMenuLinkText;
		}
		
	}

}
