package com.qmetry.qaf.example.page;

import java.util.List;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class LeavePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@FindBy(locator = "tab.teamleavelist.travel.page.loc")
	private List<QAFWebElement> tabTeamLeaveList;

	public List<QAFWebElement> getTabTeamLeaveList() {
		return tabTeamLeaveList;
	}

	public void verifyLeaveFieldsPresent(String data) {

		String[] fieldList = data.split(",");

		for (int i = 0; i < fieldList.length; i++) {

			for (QAFWebElement element : tabTeamLeaveList) {

				Validator.verifyThat(fieldList[i] + " is present", element.getText().trim().contains(fieldList[i]),
						Matchers.equalTo(true));

			}

		}
	}

}
