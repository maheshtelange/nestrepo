package com.qmetry.qaf.example.page;

import java.util.List;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.component.MenuListItem;
import com.qmetry.qaf.example.component.MenuListItem.SubMenuList;
import com.qmetry.qaf.example.steps.Utility.NestUtils;

public class MenuListPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	@FindBy(locator = "home.allLinkList.link")
	private List<MenuListItem> menuList;

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {

	}

	public List<MenuListItem> getMenuList() {
		return menuList;
	}

	public void selecMenuAndSubMenuByName(String mainMenuText, String submenuText) throws InterruptedException {

		NestUtils.waitForPageLoader();

		if (mainMenuText.equalsIgnoreCase("Publisher") || mainMenuText.equalsIgnoreCase("Reimbursement")
				|| mainMenuText.equalsIgnoreCase("R & R")) {
			NestUtils.scrollDownElement();

			NestUtils.userNavigateToMenu();

		}

		if (mainMenuText.equalsIgnoreCase("R & R")) {
			NestUtils.scrollDownElement();

			NestUtils.userNavigateToMenu();
			// CommonUtils.userNavigateToMenu();
		}

		getMenuList().get(0).waitForPresent();
		boolean mainFlag = false;
		boolean subFlag = false;

		for (MenuListItem menu : getMenuList()) {

			if (menu.getLinkText().getText().trim().equalsIgnoreCase(mainMenuText)) {

				menu.getLinkText().verifyVisible("Main Menu Link visible - " + menu.getLinkText().getText());

				menu.getLinkText().click();

				mainFlag = true;
				for (SubMenuList subMenu : menu.getSubMenuList()) {

					if (subMenu.getSubMenuLinkText().getText().trim().equals(submenuText)) {
						subMenu.getSubMenuLinkText()
								.verifyVisible("Sub Menu Link visible - " + subMenu.getSubMenuLinkText().getText());

						subMenu.getSubMenuLinkText().click();
						NestUtils.waitForPageLoader();
						subFlag = true;
						break;
					}
				}
				if (!subFlag) {

					break;
				}
			}

			NestUtils.waitForPageLoader();
		}

	}
}
