package com.qmetry.qaf.example.page;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.example.bean.TravelDataBean;

public class TravelPage {
	public void userUpdateTravelDetails(Object travelDetails) {
		TravelDataBean travelDataBean = new TravelDataBean();
		
		
		
		if (travelDetails instanceof String) {

			travelDataBean.fillFromConfig((String) travelDetails);
			  } else {
				  travelDataBean.fillData(travelDetails);
			  }

			CommonStep.sendKeys(travelDataBean.getTravelTitle(), "travel.travelTitle.text");
			
		
		
	}


}
