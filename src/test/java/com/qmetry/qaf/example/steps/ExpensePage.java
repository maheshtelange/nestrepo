package com.qmetry.qaf.example.steps;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import org.hamcrest.Matchers;
import org.openqa.selenium.Keys;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.bean.ExpenseDetailsBean;
import com.qmetry.qaf.example.steps.Utility.NestUtils;

public class ExpensePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@QAFTestStep(description = "user click on New Expense button")
	public void clickOnNewExpenseBtn() {
		NestUtils.waitForLoaderWithoutCondition();
		CommonStep.click("expense.newExpense.btn");
	}

	@QAFTestStep(description = "user fills the {0} details")
	public void userFillsTheExpenseDetails(Object expenseDetails) throws Exception {
		NestUtils.waitForLoaderWithoutCondition();
		ExpenseDetailsBean expenseDataBean = new ExpenseDetailsBean();

		if (expenseDetails instanceof String) {

			expenseDataBean.fillFromConfig(expenseDetails.toString());
			NestUtils.waitForLoaderWithoutCondition();

			CommonStep.sendKeys(expenseDataBean.getExpenseTitle(), "Expense.title.txt");
			
			CommonStep.click("Expense.expensecategory.dropdown");
			

			CommonStep.click("text.other.expense.page.loc");
			//CommonStep.sendKeys(expenseDataBean.getProjectName(), "text.projectname.expense.page.loc");
			QAFExtendedWebElement ele=new QAFExtendedWebElement("expense.project.text");
			ele.sendKeys(Keys.DOWN);
			ele.sendKeys(Keys.ENTER);

			CommonStep.sendKeys(expenseDataBean.getExpenseCategory(), "Expense.expensecategory.dropdown");
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			NestUtils.scrollToAxis(100, 800);

			CommonStep.sendKeys(expenseDataBean.getExpenseAmount(), "text.expenseamount.expense.page.loc");

			CommonStep.click("Expense.expensecategory.dropdown ");
		}

	}

	public void clickBrowseAndAttach() throws InterruptedException, IOException, AWTException {
		NestUtils.waitForLoaderWithoutCondition();

		CommonStep.verifyPresent("Expense.browser.btn");
		QAFExtendedWebElement ele = new QAFExtendedWebElement("Expense.browser.btn");
		NestUtils.clickUsingJavaScript(ele);
		StringSelection doc = new StringSelection("C:\\Users\\mahesh.telange\\Pictures\\Saved Pictures\\text.txt");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(doc, null);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}

	@QAFTestStep(description = "user click on {Submit} button")
	public void userClickOnSubmitButton(String button) {
//		
//		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("submit.Expense.Btn"));
		
		QAFExtendedWebElement	element=new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString("submit.Expense.Btn"),
							button)); 
		NestUtils.scrollUpToElement(element); 

		Validator.verifyThat(button+"Button is present", element.verifyPresent(),Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user should see the attached file")
	public void userShouldSeeTheAttachedFile() {
		NestUtils.waitForPageLoader();

		Validator.verifyThat("file attached successfully", CommonStep.verifyPresent("text.attachmnet.common.page.loc"),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user click on browse to attach a file")
	public void userClickOnBrowseToAttachAFile() throws IOException, InterruptedException, AWTException {
		NestUtils.waitForPageLoader();

		CommonStep.verifyPresent("Expense.browser.btn");
		QAFExtendedWebElement ele = new QAFExtendedWebElement("Expense.browser.btn");
		NestUtils.clickUsingJavaScript(ele);
		CommonStep.click("Expense.browser.btn");
		StringSelection ss = new StringSelection("C:\\Users\\mahesh.telange\\Pictures\\Saved Pictures\\text.txt");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}

	@QAFTestStep(description = "user click on New Expense button on expense page")
	public void userClickOnButtonOnExpensePage() {
		NestUtils.waitForPageLoader();
		CommonStep.click("Expense.NewExpense.btn");
	}

}
