package com.qmetry.qaf.example.steps;

import org.hamcrest.Matchers;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.page.MenuListPage;
import com.qmetry.qaf.example.steps.Utility.NestUtils;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@QAFTestStep(description = "user Navigate on Home page {title}")
	public void verifyHomePage(String Title) {
		Validator.verifyThat("Verify home page Title", driver.getTitle(), Matchers.containsString(Title));
		NestUtils.waitForPageLoader();
	}

	@QAFTestStep(description = "user scrolls down menu")
	public static void userScrollsDownMenu() {
		Actions dragger = new Actions(NestUtils.getDriver());

		QAFExtendedWebElement scrollBar = new QAFExtendedWebElement("home.navigation.scrollbar");
		Point scrollBarLocation = scrollBar.getLocation();

		dragger.moveToElement(scrollBar).clickAndHold().moveByOffset(0, scrollBarLocation.getY() + 300).release()
				.perform();

	}

	@QAFTestStep(description = "user selects {mainMenu} main menu link and {subMenu} sub menu link")
	public void selecMenuAndSubMenu(String mainMenuText, String submenuText) throws InterruptedException {

		NestUtils.waitForPageLoader();
		NestUtils.userNavigateToMenu();
		MenuListPage listPage = new MenuListPage();
		listPage.selecMenuAndSubMenuByName(mainMenuText, submenuText);
	}

	@QAFTestStep(description = "Click on Relationship Declaration in the profile page")
	public void clickOnRelationshipDeclaration() {
		NestUtils.waitForPageLoader();

		NestUtils.scrollToAxis(100, 800);
		QAFExtendedWebElement element = new QAFExtendedWebElement("profile.relation.text");
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "Click on check button")
	public void clickOnCheckBox() {
		CommonStep.click("profile.click.checkBox");

	}

	@QAFTestStep(description = "user switches to managerView")
	public void clickOnViewBtn() {
		NestUtils.waitForPageLoader();
		CommonStep.waitForVisible("click.viewAll.btn", 20);
		CommonStep.click("click.viewAll.btn");
	}

	@QAFTestStep(description = "user clicks on View All link in the Request section")
	public void userClicksOnViewAllLinkInTheRequestSection() {
		NestUtils.waitForPageLoader();
		CommonStep.click("home.link.viewAllRequests");
		Validator.verifyThat("Verify Leave request Page", CommonStep.getText("home.txt.travelRequest"),
				Matchers.containsString("Team Leave List"));
		NestUtils.waitForPageLoader();
		NestUtils.iNavigateBack();
	}

	@QAFTestStep(description = "user should be navigated to Respective page")
	public void userShouldBeNavigatedToRespectedPage() {

		NestUtils.waitForPageLoader();
		CommonStep.click("home.link.expenseRequests");
		CommonStep.click("home.link.viewAllRequests");
		Validator.verifyThat("Verify Leave request Page", CommonStep.getText("home.txt.expenseRequest"),
				Matchers.containsString("Team Reimbursement List"));
		NestUtils.waitForPageLoader();
		NestUtils.iNavigateBack();
		NestUtils.waitForPageLoader();
		CommonStep.click("home.link.travelRequests");
		CommonStep.click("home.link.viewAllRequests");
		Validator.verifyThat("Verify Leave request Page", CommonStep.getText("home.txt.travelRequest"),
				Matchers.containsString("Travel Requests"));
	}

}
