package com.qmetry.qaf.example.steps;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.page.LeavePage;
import com.qmetry.qaf.example.steps.Utility.NestUtils;

public class LeavePageStep {
	LeavePage leavePage = new LeavePage();

	@QAFTestStep(description = "user should see {0} for {1} for leave page")
	public void userVerifyBlockAndFields(String data, String loc) {

		String[] fieldList = data.split(",");

		for (int i = 0; i < fieldList.length; i++) {
			Validator.verifyThat(new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString("tab.teamleavelist.travel.page.loc"), fieldList[i]))
							.isPresent(),
					Matchers.equalTo(true));
		}

	}

	@QAFTestStep(description = "user verify {0} fields present")
	public void userVerifyFieldsFor(String data) {
		NestUtils.waitForPageLoader();

		leavePage.verifyLeaveFieldsPresent(data);

	}
}
