package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.QAFTestStep;

public class LoginCommonStep {
	LoginPage login = new LoginPage();

	@QAFTestStep(description = "user loging with userName as {UserName} and password as {pasword}")
	public void userLogin(String userName, String pasword) {
		login.open();
		LoginPage.userEnterValidUserNameAndPass(userName, pasword);
		LoginPage.clickOnLoginBtn();

	}

}
