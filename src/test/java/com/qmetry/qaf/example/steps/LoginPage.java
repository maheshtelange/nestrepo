package com.qmetry.qaf.example.steps;

import org.hamcrest.Matchers;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.steps.Utility.NestUtils;

public class LoginPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {

	}

	@QAFTestStep(description = "user launches the application")
	public void open() {
		driver.get("/");

	}

	@QAFTestStep(description = "user enter username as {userName} and user enter password as {Psw}")
	public static void userEnterValidUserNameAndPass(String Uname, String Pass) {

		CommonStep.sendKeys(Uname, "login.username.txt");

		CommonStep.sendKeys(Pass, "login.password.txt");

	}

	@QAFTestStep(description = "user click on login button")
	public static void clickOnLoginBtn() {

		CommonStep.click("login.btn");

	}

	@QAFTestStep(description = "user get a message {error msg}")
	public void userGetsAMessage(String str0) {

		Validator.verifyThat("validation for login page when username and password is left blank",
				CommonStep.getText("login.error.msg"), Matchers.containsString(str0));

	}

	@QAFTestStep(description = "user click on Logout Button")
	public void clickOnLogOutBtn() {
		NestUtils.waitForPageLoader();

		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("login.logout.btn"));
	}

}
