package com.qmetry.qaf.example.steps;

import java.util.List;

import org.hamcrest.Matchers;
import org.omg.CORBA.PUBLIC_MEMBER;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.steps.Utility.NestUtils;

public class PublishStep {
	public List<QAFWebElement> allTextField() {
		return NestUtils.getQAFWebElements("publish.textfields.text");
	}

	public List<QAFWebElement> allDropDown() {
		return NestUtils.getQAFWebElements("publish.dropdown.list");
	}

	@QAFTestStep(description="user click on Create button")
	public void clickOnCreateBtn() {
		NestUtils.waitForPageLoader();
	NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("publish.creat.Btn"));
		
	}
	@QAFTestStep(description = "user verify title of create post {createpost} page")
	public void userVerifyTitleOfPage(String title) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("publish.createPost.title"),
				title));
		element.waitForVisible(5000);
		Validator.verifyThat(title + " is present", element.verifyPresent(),
				Matchers.equalTo(true));
		

	}
	@QAFTestStep(description = "user verify All the given Text field")
	public void verifyTextField() {
		
		
		for (int i = 0; i < allTextField().size(); i++) {
			allTextField().get(i).verifyPresent();
		}
	}
	@QAFTestStep(description = "user verify {0} given below")
	public void verifyDropdowns(String value) {

		for (int i = 0; i < allDropDown().size(); i++) {
			String result = allDropDown().get(i).getAttribute("class");
			
		}
	}
	@QAFTestStep(description = "user verify {0} button")
	public void userVerifyButton(String text) {
		

	}
}
