package com.qmetry.qaf.example.steps;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.steps.Utility.NestUtils;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class RR_RequestStep {
	public static final String TO_DATE_RNR_REQUESTPAGE = "date.to.rnrrequestpage";
	public static final String SELECT_NOMINEE = "Select Nominee";
	public static final String SELECT_PROJECT = "Select Project";
	public static final String MANAGER_STATUS = "Manager Status";
	public static final String HR_STATUS = "HR Status";
	public static final String PENDING = "Pending";
	public static final String HR_ADMIN = "HR/Admin";
	public static final String PATON_BACK = "Pat On Back";
	public static final String BRIGHT_SPARK = "Bright Spark";

	static Calendar calender = Calendar.getInstance();
	static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
	static final String AVAILABLE_FIELDS_BUTTON = "button.avlfields.nominatepages";
	static final String INPUT_TEXTAREA = "imput.textarea";
	static final String LINK_POB = "link.PatOnBack.MyRewardPageListPage.rnr";

	@QAFTestStep(description = "user selects {0} link")
	public void userSelectsBrightSparkLink(String linkName) {

		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("link.subsubmenu.rnr.page.loc"), linkName));

		element.waitForPresent();
		element.verifyPresent("Link - " + linkName);
		element.click();
		NestUtils.waitForPageLoader();

	}

	@QAFTestStep(description = "user verify the bright spark")
	public void userVerifyTheBrightSpark() {
		CommonStep.verifyPresent("text.nominee.rnr.page.loc");
	}

	@QAFTestStep(description = "user verify Pagination should be present")
	public void verifyPaginationMyPostPage() {
		NestUtils.scrollToAxis(0, 500);

		Validator.verifyThat("Pagination is present", verifyPresent("rnr.pagination.list"), Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user verify title of {0} page")
	public void userVerifyTitleOfPage(String title) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("text.title.landed.page.loc"), title));
		element.waitForVisible(5000);
		Validator.verifyThat(title + " is present", element.verifyPresent(), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user should see {0} for {1}")
	public void userSeeBlockAndFields(String data, String loc) {

		String[] fieldList = data.split(",");
		for (int i = 0; i < fieldList.length; i++) {
			Validator.verifyThat(new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("link.tabs.leave.page.loc"), fieldList[i]))
							.isPresent(),
					Matchers.equalTo(true));
		}

	}

}
