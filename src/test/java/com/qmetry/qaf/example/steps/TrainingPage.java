package com.qmetry.qaf.example.steps;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.hamcrest.Matchers;
import org.openqa.selenium.Keys;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;

import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.steps.Utility.NestUtils;

public class TrainingPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@QAFTestStep(description = "user click on Add/View Members button on training page")
	public void userClickOnAddViewMembersButton() {
		NestUtils.scrollToAxis(50, 700);
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("training.addviewmwmber.btn"));

	}

	@QAFTestStep(description = "user select {0} where name is {1}")
	public void userSelection(String filterLabel, String filterOption) throws InterruptedException, AWTException {

	}

	@QAFTestStep(description = "user click on browse attachment")
	public void userClickOnBrowseAttachment() {

	}

	@QAFTestStep(description = "user clicks on any upcoming training to be taken")
	public void clickOnUpcomingTraining() {
		NestUtils.waitForPageLoader();
		CommonStep.click("training.calender.click");

	}

	@QAFTestStep(description = "user click on browse to attach a file")
	public void clickBrowseAndAttach() throws InterruptedException, IOException, AWTException {

	}

	@QAFTestStep(description = "user click on Yes button")
	public void userClickOnYesButton() {
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("training.yes.btn"));

	}

	@QAFTestStep(description = "user clicks on deregister icon")
	public void userClicksOnDeregisterIcon() {
		NestUtils.waitForPageLoader();
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("training.cross.icon"));

	}

	@QAFTestStep(description = "user verifies deregister message in training page")
	public void verifyMessageInTraining() {
		NestUtils.waitForPageLoader();

		Validator.verifyThat("Employee is deregistered successfully", CommonStep.getText("label.status.common")
				.toString().trim().equals("Employee is deregistered successfully"), Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user clicks on Register button")
	public void clickRegisterBtn() {
		NestUtils.waitForPageLoader();
		CommonStep.click("training.select.btn");
		QAFWebElement ele=new QAFExtendedWebElement("training.add/view.text");
		ele.sendKeys(Keys.DOWN);
		ele.sendKeys(Keys.ENTER);
		CommonStep.click("training.register.btn");

			}

	@QAFTestStep(description = "user delete the uploaded attachment")
	public void userDeleteTheUploadedAttachment() {
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("training.deleteUpload.icon"));

	}

	@QAFTestStep(description = "user verify the attachment deleted")
	public void userVerifyAttachmentDeleted() {

	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@QAFTestStep(description = "user click on browse to attach a file on Training page")
	public void clickOnBrowserAttachFile() throws Exception {
		NestUtils.scrollToAxis(50, 700);
		CommonStep.click("training.browser.btn");
		StringSelection doc = new StringSelection("C:\\Users\\mahesh.telange\\Pictures\\Saved Pictures\\text.txt");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(doc, null);
		Robot robot = new Robot();

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}

}