package com.qmetry.qaf.example.steps;

import java.util.List;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.page.TravelPage;
import com.qmetry.qaf.example.steps.Utility.NestUtils;

public class TravelPageStep {
	TravelPage travel = new TravelPage();
	@FindBy(locator = "travel.buttons")
	private List<QAFExtendedWebElement> travelbtn;

	@QAFTestStep(description = "user click on {New Travel Request} NewTravelrequest button")
	public void userClickOnButton(String str0) {

		CommonStep.click("btn.newTravelRequest.loc");

	}

	@QAFTestStep(description = "user verify asterick sign for {data} mandatory Fields")
	public void userVerifyAsterickSign(String data) {

		NestUtils.waitForPageLoader();
		NestUtils.verifyAstrikOnMandatoryFields(data);

	}

	@QAFTestStep(description = "user should see {0} for {1} for travel page")
	public void userShouldSeeForForTravelPage(String field, String loc) {
		String[] fieldList = field.split(",");
		for (int i = 0; i < fieldList.length; i++) {
			Validator.verifyThat(new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("travel.textTitle.txt"), fieldList[i]))
							.isPresent(),
					Matchers.equalTo(true));
		}

	}

	@QAFTestStep(description = "user click on edit icon")
	public void userClickOnEditBtn() {
		NestUtils.waitForPageLoader();
		QAFExtendedWebElement ele = new QAFExtendedWebElement("travel.edit.btn");
		NestUtils.clickUsingJavaScript(ele);

	}

	@QAFTestStep(description = "user update {travelD} details")
	public void userUpdateDetailsDetails(Object travelDetails) {
		travel.userUpdateTravelDetails(travelDetails);
	}

	@QAFTestStep(description = "user click on {yes} button")
	public void yesbutton(String buttonName) {
		NestUtils.waitForPageLoader();

		NestUtils.scrollToAxis(50, 700);
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("travel.yes.btn"));

	}

	@QAFTestStep(description = "user click on {Submit} button on travel")
	public void userClickOnSubmitButton(String str0) {

		NestUtils.waitForPageLoader();
		NestUtils.scrollToAxis(80, 500);
		CommonStep.click("travel.submit.btn");

	}

	@QAFTestStep(description = "user verifies {message} message")
	public void verifyMessage(String message) {
		NestUtils.waitForPageLoader();
		/*Validator.verifyThat("verify message", CommonStep.getText("travel.updatemsg.msg"),
				Matchers.containsString(message));
*/
	}

	@QAFTestStep(description = "user click on delete icon")
	public void clickOnDeleteBtn() {
		NestUtils.waitForPageLoader();
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("travel.delete.btn"));
	}

	@QAFTestStep(description = "user click on Travel request tab")
	public void userClickOnTravelTab() {
		NestUtils.waitForPageLoader();
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("travel.request.tab"));

	}

	@QAFTestStep(description = "user click on eye icon")
	public void userClickOneyeIcon() {
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("travel.eye.icon"));
	}

	@QAFTestStep(description = "user see the button {0}")
	public void userSeeBtns(String button) {
		NestUtils.waitForPageLoader();
		//NestUtils.scrollUpToElement();
		for (int i = 0; i < travelbtn.size(); i++) {
//			Validator.verifyThat(new QAFExtendedWebElement(String
//					.format(ConfigurationManager.getBundle().getString("link.tabs.leave.page.loc"), travelbtn.get(i)))
//							.isPresent(),
//					Matchers.equalTo(true));
		}

	}

}
