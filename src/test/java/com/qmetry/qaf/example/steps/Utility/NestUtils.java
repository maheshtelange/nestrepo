package com.qmetry.qaf.example.steps.Utility;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class NestUtils {
	static WebDriverWait wait = new WebDriverWait(NestUtils.getDriver(), 30);
	public static final String LOADER = "Login.Loader.img";
	public static final String HTML_COMMON = "html.common";

	public static void waitForPageLoader() {

		CommonStep.waitForNotVisible(LOADER, 120);
		waitForLocToBeClickable(new QAFExtendedWebElement("login.logout.btn"));
	}

	public static QAFExtendedWebDriver getDriver() {
		return new WebDriverTestBase().getDriver();
	}

	public static void waitForLoaderWithoutCondition() {
		QAFExtendedWebElement element = new QAFExtendedWebElement("Login.Loader.img");
		CommonStep.waitForNotVisible(LOADER);

	}

	public static void waitForLocToBeClickable(QAFWebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public static void clickUsingJavaScript(WebElement ele) {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", ele);
	}

	public static void userNavigateToMenu() throws InterruptedException {
		QAFExtendedWebElement element = new QAFExtendedWebElement("home.icon.img");
		clickUsingJavaScript(element);
		QAFTestBase.pause(5000);

	}

	public static List<QAFWebElement> getQAFWebElements(String loc) {
		List<QAFWebElement> listWebElement = new QAFExtendedWebElement(NestUtils.HTML_COMMON).findElements(loc);

		return listWebElement;
	}

	public static void scrollDownElement() {
		Actions dragger = new Actions(NestUtils.getDriver());

		QAFExtendedWebElement scrollBar = new QAFExtendedWebElement("home.navigation.scrollbar");
		Point scrollBarLocation = scrollBar.getLocation();

		dragger.moveToElement(scrollBar).clickAndHold().moveByOffset(0, scrollBarLocation.getY() + 300).release()
				.perform();
		QAFTestBase.pause(2000);
	}

	public static void scrollToAxis(int x, int y) {
		((JavascriptExecutor) new WebDriverTestBase().getDriver())
				.executeScript("window.scrollTo(" + x + ", " + y + ");");
	}

	public static void clickUsingJavaScript(QAFExtendedWebElement ele) {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", ele);
	}

	public static void scrollUpToElement(QAFWebElement ele) {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].scrollIntoView()", ele);
	}

	public static void iNavigateBack() {
		NestUtils.waitForLoaderWithoutCondition();
		NestUtils.getDriver().navigate().back();
	}

	public static void verifyAstrikOnMandatoryFields(String data) {

		String[] fieldList = data.split(",");
		Reporter.log("String received" + Arrays.toString((fieldList)));
		for (int i = 0; i < fieldList.length; i++) {
			Validator.verifyThat(new QAFExtendedWebElement(String
					.format(ConfigurationManager.getBundle().getString("icon.astrik.common.page.loc"), fieldList[i]))
							.isPresent(),
					Matchers.equalTo(true));
		}

	}

}
